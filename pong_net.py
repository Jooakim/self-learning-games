import sys, pygame, random
import numpy as np

pygame.init()

size = width, height = 840, 480
speed = [1, 1]


# Constants
WHITE = 255, 255, 255
BLACK = 0, 0, 0
BAR_SPEED = 6
BAR_SIZE = [15,60]
BAR1_X = 0
BOT_X = width-BAR_SIZE[0]
BOT_SPEED = 1
BOT_UPDATE_INTERVAL = 30
BALL_SIZE = 15


# Rewards for nn
HIT_REWARD = 0
LOSE_REWARD = -1
SCORE_REWARD = 1

screen = pygame.display.set_mode(size)

# Moveable objects
bar_sur = pygame.Surface((BAR_SIZE[0],BAR_SIZE[1]))
bar = bar_sur.convert()
bar.fill(WHITE)

ball_sur = pygame.Surface((BALL_SIZE, BALL_SIZE))
ball = pygame.draw.rect(ball_sur, WHITE,(0, 0, BALL_SIZE, BALL_SIZE))
ball_conv = ball_sur.convert()


class GameState: 

    def __init__(self):
        # Initial values
        self.ball_x, self.ball_y = width/2-BALL_SIZE, height/2-BALL_SIZE
        self.ball_vert_speed = 5
        self.ball_horiz_speed = -5
        self.player1_score = 0
        self.player2_score = 0

        self.vertical_speed = 0
        self.bot_vert_speed = 0
        self.start_speed = BOT_UPDATE_INTERVAL - 1
        self.bot_time = 0
        self.bot_y = 0
        self.bar1_y = 0
        self.font = pygame.font.SysFont("calibri",40)
        #self.clock = pygame.time.Clock()

        self.bar_ = pygame.draw.rect(bar_sur, WHITE, (BAR1_X, self.bar1_y, BAR_SIZE[0], BAR_SIZE[1]))



    def ball_hit_bar(self):
        if self.ball_horiz_speed < 0:
            return self.ball_x <= BAR1_X+BAR_SIZE[0] and self.ball_y+BALL_SIZE >= self.bar1_y and self.ball_y <= self.bar1_y+BAR_SIZE[1]
        else:
            return self.ball_x+BALL_SIZE >= BOT_X and self.ball_y+BALL_SIZE >= self.bot_y and self.ball_y <= self.bot_y+BAR_SIZE[1]

    def start_in_middle(self):
        self.bot_y = height/2 - BAR_SIZE[1]/2
        self.bar1_y = height/2 - BAR_SIZE[1]/2
        self.ball_horiz_speed *= -1
        return width/2 - BALL_SIZE/2, height/2 - BALL_SIZE/2

    def move_one_frame(self,movement):

        reward = 0

        # Make the scores visible
        score1 = self.font.render(str(self.player1_score), True,(255,255,255))
        score2 = self.font.render(str(self.player2_score), True,(255,255,255))

        if movement[1] == 1:
            self.vertical_speed = -BAR_SPEED
        elif movement[2] == 1: 
            self.vertical_speed = BAR_SPEED
        else:
            self.vertical_speed = 0

        self.bar1_y += self.vertical_speed

        if self.bar1_y < 0:
            self.bar1_y = 0
        elif self.bar1_y + BAR_SIZE[1] > height:
            self.bar1_y = height - BAR_SIZE[1]

        # Bot aka player 2 movement
        self.bot_time += BOT_SPEED;
        if self.ball_x >= width/2:
            if self.bot_time%(BOT_UPDATE_INTERVAL - self.start_speed) == 0:
                self.bot_time = 0
                if self.ball_y > self.bot_y + BAR_SIZE[1]:
                    self.bot_vert_speed = BAR_SPEED
                elif self.ball_y < self.bot_y:
                    self.bot_vert_speed = -BAR_SPEED
                else:
                    self.bot_vert_speed = 0

            self.bot_y += self.bot_vert_speed
            if self.bot_y < 0:
                self.bot_y = 0
            elif self.bot_y + BAR_SIZE[1] > height:
                self.bot_y = height - BAR_SIZE[1]
        else:
            self.bot_vert_speed = 0

        # Keep FPS at 60
        #self.clock.tick(60)


        # Move the ball
        self.ball_x += self.ball_horiz_speed
        self.ball_y += self.ball_vert_speed

        if self.ball_y < 0 or self.ball_y+BALL_SIZE > height:
            self.ball_vert_speed *= -1

        if self.ball_hit_bar():
            self.start_speed = 0
            self.ball_horiz_speed *= -1
        elif self.ball_horiz_speed < 0:
            if self.ball_x < BAR1_X+BAR_SIZE[0]:
                reward = LOSE_REWARD 
                self.player2_score += 1
                self.start_speed = BOT_UPDATE_INTERVAL - 1
                self.ball_x, self.ball_y = self.start_in_middle()
        else:
            if self.ball_x+BALL_SIZE/2 > BOT_X:
                reward = SCORE_REWARD 
                self.player1_score += 1
                self.ball_x, self.ball_y = self.start_in_middle()

        game_over = False
        if self.player1_score >= 20 or self.player2_score >= 20:
            game_over = True


        screen.fill(BLACK)
        screen.blit(ball_conv, (self.ball_x,self.ball_y))
        screen.blit(bar, (BAR1_X, self.bar1_y))
        screen.blit(bar, (BOT_X, self.bot_y))
        screen.blit(score1,(width/2-80,0))
        screen.blit(score2,(width/2+70,0))

        pygame.display.update()

        pixel_data = pygame.surfarray.pixels3d(screen)

        return pixel_data, reward, game_over

        




