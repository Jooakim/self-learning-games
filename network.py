import tensorflow as tf
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import numpy as np
import random
import pong_net
import matplotlib.pyplot as plt
from collections import deque

# Constants
INITIAL_EPSILON = 1.0
ACTIONS = 3 # Nbr of allowed actions
GAMMA = 0.99 # decay rate of past observations
OBSERVE = 500. # timesteps to observe before training
EXPLORE = 500. # frames over which to anneal epsilon
FINAL_EPSILON = 0.05 # final value of epsilon
INITIAL_EPSILON = 1.0 # starting value of epsilon
REPLAY_MEMORY = 590000 # number of previous transitions to remember
BATCH = 32 # size of minibatch
K = 1 # only select an action every Kth frame, repeat prev for others

def conv2d(inp, weights, stride):
    return tf.nn.conv2d(inp, weights, strides = [1,stride,stride,1], padding = "SAME")

def create_network():
    W_conv1 = tf.truncated_normal([8, 8, 4, 32], stddev=0.01)
    b_conv1 = tf.Variable(tf.constant(0.01, shape = [32]))

    W_conv2 = tf.truncated_normal([4, 4, 32, 64], stddev=0.01)
    b_conv2 = tf.Variable(tf.constant(0.01, shape = [64]))
    print b_conv2
    
    W_conv3 = tf.truncated_normal([3, 3, 64, 64], stddev=0.01)
    b_conv3 = tf.Variable(tf.constant(0.01, shape = [64]))

    W_fc1 = tf.truncated_normal([1600, 512], stddev=0.01)
    b_fc1 = tf.Variable(tf.constant(0.01, shape = [512]))

    W_fc2 = tf.truncated_normal([512, ACTIONS], stddev=0.01)
    b_fc2 = tf.Variable(tf.constant(0.01, shape = [ACTIONS]))


    # The input layer
    s = tf.placeholder("float", [None, 80, 80, 4])

    # Hidden layers
    h_conv1 = tf.nn.relu(conv2d(s, W_conv1, 4) + b_conv1)
    h_pool1 = tf.nn.max_pool(h_conv1, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = "SAME")

    h_conv2 = tf.nn.relu(conv2d(h_pool1 , W_conv2, 2) + b_conv2)

    h_conv3 = tf.nn.relu(conv2d(h_conv2, W_conv3, 1) + b_conv3)

    h_conv3_flat = tf.reshape(h_conv3, [-1, 1600])

    h_fc1 = tf.nn.relu(tf.matmul(h_conv3_flat, W_fc1) + b_fc1)

    readout = tf.matmul(h_fc1, W_fc2) + b_fc2
    print readout

    return s, readout, h_fc1

def train_network(s, readout, h_fc1, session):
    # Cost function
    a = tf.placeholder("float", [None, ACTIONS])
    y = tf.placeholder("float", [None])
    readout_action = tf.reduce_sum(tf.mul(readout,a), reduction_indices = 1)
    cost = tf.reduce_mean(tf.square(y - readout_action))
    train_step = tf.train.AdamOptimizer(1e-6).minimize(cost)

    
    game_state = pong_net.GameState()

    # Store history
    D = deque()

    
    # Preprocessing on image
    do_nothing = np.zeros([ACTIONS])
    pixel_data, reward, game_flag = game_state.move_one_frame(do_nothing)
    pixel_data = cv2.cvtColor(cv2.resize(pixel_data,(80,80)),cv2.COLOR_BGR2GRAY)
    ret, pixel_data = cv2.threshold(pixel_data,1,255,cv2.THRESH_BINARY)
    s_t = np.stack((pixel_data, pixel_data, pixel_data, pixel_data), axis = 2)


    saver = tf.train.Saver()
    sess.run(tf.initialize_all_variables())
    checkpoint = tf.train.get_checkpoint_state("saved_networks")
    if checkpoint and checkpoint.model_checkpoint_path:
        saver.restore(sess, checkpoint.model_checkpoint_path)
        print "Successfully loaded:", checkpoint.model_checkpoint_path
    else:
        print "Could not find old network weights"
            


    epsilon = INITIAL_EPSILON
    t = 0

    while True:
        readout_t = readout.eval(feed_dict = {s : [s_t]})[0]
        a_t = np.zeros([ACTIONS])

        if random.random() <= epsilon or t <= OBSERVE:
            action_index = random.randrange(ACTIONS)
            a_t[random.randrange(ACTIONS)] = 1
        else:
            action_index = np.argmax(readout_t)
            a_t[action_index] = 1

        
        if epsilon > FINAL_EPSILON and t > OBSERVE:
            epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE


        for i in range(0,K):
            x_t1_col = 0
            x_t1_col, r_t, terminal = game_state.move_one_frame(a_t)
            x_t1 = cv2.cvtColor(cv2.resize(x_t1_col, (80, 80)), cv2.COLOR_BGR2GRAY)
            ret, x_t1 = cv2.threshold(x_t1, 1, 255, cv2.THRESH_BINARY)
            x_t1 = np.reshape(x_t1, (80, 80, 1))
            s_t1 = np.append(x_t1, s_t[:,:,1:], axis = 2)

            # Store data in deque
            D.append((s_t, a_t, r_t, s_t1, terminal))
            if len(D) > REPLAY_MEMORY:
                D.popleft()

        if t > OBSERVE:
            minibatch = random.sample(D, BATCH)

            s_j_batch = [d[0] for d in minibatch]
            a_batch = [d[1] for d in minibatch]
            r_batch = [d[2] for d in minibatch]
            s_j1_batch = [d[3] for d in minibatch]

            y_batch = []
            readout_j1_batch = readout.eval(feed_dict = {s : s_j1_batch})
            for i in range(0, len(minibatch)):

                if minibatch[i][4]:
                    y_batch.append(r_batch[i])
                else:
                    y_batch.append(r_batch[i] + GAMMA * np.max(readout_j1_batch[i]))

            train_step.run(feed_dict = {y : y_batch, a : a_batch, s : s_j_batch})


        s_t = s_t1
        t += 1

        if t % 10000 == 0:
            saver.save(sess, 'saved_networks/' + 'PONG' + '-dqn', global_step = t)

        print "TIMESTEP", t,  "/ EPSILON", epsilon, "/ ACTION", action_index, "/ REWARD", r_t, "/ Q_MAX %e" % np.max(readout_t)

#plt.imshow(pixel_data)
#plt.show()

sess = tf.InteractiveSession()
s, readout,h_fc1 = create_network()
train_network(s,readout,h_fc1,sess)


