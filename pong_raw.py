import sys, pygame, random
import numpy as np

pygame.init()

size = width, height = 840, 480
speed = [1, 1]


# Constants
WHITE = 255, 255, 255
BLACK = 0, 0, 0
BAR_SPEED = 5
BAR_SIZE = [15,60]
BAR1_X = 0
BOT_X = width-BAR_SIZE[0]
BOT_SPEED = 1
BOT_UPDATE_INTERVAL = 20



BALL_SIZE = 15

# Initial values
ball_x, ball_y = width/2-BALL_SIZE, height/2-BALL_SIZE
ball_vert_speed = 5
ball_horiz_speed = -5
player1_score = 0
player2_score = 0

vertical_speed = 0
bot_vert_speed = 0
bot_time = 0
bot_y = 0
bar1_y = 0
screen = pygame.display.set_mode(size)
font = pygame.font.SysFont("calibri",40)
clock = pygame.time.Clock()


# Moveable objects
bar1_sur = pygame.Surface((BAR_SIZE[0],BAR_SIZE[1]))
bar1_ = pygame.draw.rect(bar1_sur, WHITE, (BAR1_X, bar1_y, BAR_SIZE[0], BAR_SIZE[1]))
bar1_conv = bar1_sur.convert()

ball_sur = pygame.Surface((BALL_SIZE, BALL_SIZE))
ball = pygame.draw.rect(ball_sur, WHITE,(0, 0, BALL_SIZE, BALL_SIZE))
ball_conv = ball_sur.convert()


def get_playfield():
    return pygame.Surfarce.get_view(1)

def ball_hit_bar():
    if ball_horiz_speed < 0:
        return ball_x <= BAR1_X+BAR_SIZE[0] and ball_y+BALL_SIZE >= bar1_y and ball_y <= bar1_y+BAR_SIZE[1]
    else:
        return ball_x+BALL_SIZE >= BOT_X and ball_y+BALL_SIZE >= bot_y and ball_y <= bot_y+BAR_SIZE[1]
        return ball_x >= BOT_X 

def start_in_middle():
    return width/2 - BALL_SIZE/2, height/2 - BALL_SIZE/2

gameOver = False
while gameOver == False:

    # Make the scores visible
    score1 = font.render(str(player1_score), True,(255,255,255))
    score2 = font.render(str(player2_score), True,(255,255,255))

    # Movement for player one bar
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                vertical_speed = -BAR_SPEED
            elif event.key == pygame.K_DOWN:
                vertical_speed = BAR_SPEED
            elif event.key == pygame.K_SPACE:
                if ball_horiz_speed == 0:
                    ball_horiz_speed = np.sign(random.random() - 0.5)*BAR_SPEED
                    ball_vert_speed = np.sign(random.random() - 0.5)*BAR_SPEED
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                vertical_speed = 0
            elif event.key == pygame.K_DOWN:
                vertical_speed = 0
    bar1_y += vertical_speed
    if bar1_y < 0:
        bar1_y = 0
    elif bar1_y + BAR_SIZE[1] > height:
        bar1_y = height - BAR_SIZE[1]

    # Bot aka player 2 movement
    bot_time += BOT_SPEED;
    if bot_time%BOT_UPDATE_INTERVAL == 0:
        bot_time = 0
        if ball_y > bot_y + BAR_SIZE[1]:
            bot_vert_speed = BAR_SPEED
        elif ball_y < bot_y:
            bot_vert_speed = -BAR_SPEED
        else:
            bot_vert_speed = 0

    bot_y += bot_vert_speed
    if bot_y < 0:
        bot_y = 0
    elif bot_y + BAR_SIZE[1] > height:
        bot_y = height - BAR_SIZE[1]

    # Keep FPS at 60
    clock.tick(60)


    # Move the ball
    ball_x += ball_horiz_speed
    ball_y += ball_vert_speed

    if ball_y < 0 or ball_y+BALL_SIZE > height:
        ball_vert_speed *= -1

    if ball_hit_bar():
        ball_horiz_speed *= -1
    elif ball_horiz_speed < 0:
        if ball_x < BAR1_X+BAR_SIZE[0]:
            player2_score += 1
            ball_horiz_speed = 0
            ball_vert_speed = 0
            ball_x, ball_y = start_in_middle()
    else:
        if ball_x+BALL_SIZE/2 > BOT_X:
            player1_score += 1
            ball_horiz_speed = 0
            ball_vert_speed = 0
            ball_x, ball_y = start_in_middle()
    

    screen.fill(BLACK)
    screen.blit(ball_conv , (ball_x,ball_y))
    screen.blit(bar1_conv, (BAR1_X, bar1_y))
    screen.blit(bar1_conv, (BOT_X, bot_y))
    screen.blit(score1,(width/2-80,0))
    screen.blit(score2,(width/2+70,0))

    pygame.display.update()




